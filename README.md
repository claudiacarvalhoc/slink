# sLink - Estrutura de Disseminação de dados microbiológicos com controle de acessos #

### Introdução ###

Com o constante crescimento tecnologico na investigacão cientca em ciências biológicas, a necessidade
de armazenamento e gestão de informacão recolhida e uma preocupac~ao cada vez mais
real nos investigadores. Recentemente, os novos métodos de sequenciacão de DNA permitem a
determinac~ao total de genomas bacterianos em poucos dias, gerando quantidades de dados na
ordem das centenas de megabytes por amostra analisado. Esta constante evoluc~ao proporciona
um grande benefcio a area da Saude Humana, nomeadamente na Microbiologia Clnica, onde
uma panoplia de metodos de analise de sequência de DNA foram desenvolvidos para a identicacão bacteriana. Estes metodos s~ao utilizados na deteccão de surtos no estudo da evolucão
da resist^encia a anti-microbianos em bactérias patogenicas. Existem inumeras bases de dados
disponveis na Internet correspondentes a diferentes metodos de tipagem microbiana baseada em
sequências de DNA. Como exemplo destas temos o MLST[1] e PubMLST.org[2]. Cada laboratorio
de investigacão ou ag^encia governamental possui tambem varias bases de dados privadas das suas
amostras. Um dos desafios que encontra por se resolver e a integrac~ao destes dados públicos e
privados. Presentemente, a falta de padronizacão de interfaces e formatos de dados leva a que
este processo esteja limitado a uma morosa integracão manual dos dados disponveís nas diversas
bases de dados.

### Autora: ###

* Claudia Carvalho - [Linkedin](https://www.linkedin.com/in/claudiacarvalho19)

### Orientado por: ###

* Cátia Vaz – cvaz@cc.isel.ipl.pt
* João Carriço – jcarrico@kdbio.inesc-id.pt
* Alexandre Francisco - aplf@ist.utl.pt

__________________________________________________________________________________________

# sLink - Structure Dissemination of microbiological data with access control #

### Author: ###

* Claudia Carvalho - [Linkedin](https://www.linkedin.com/in/claudiacarvalho19)

### Guided by: ###

* Cátia Vaz – cvaz@cc.isel.ipl.pt
* João Carriço – jcarrico@kdbio.inesc-id.pt
* Alexandre Francisco - aplf@ist.utl.pt